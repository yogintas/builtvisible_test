const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('styles', () => {
  gulp.src('./assets/styles/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/styles/'));
});

gulp.task('default', () => {
  gulp.watch('./assets/styles/scss/*.scss', ['styles']);
});
