document.addEventListener('DOMContentLoaded', () => {
  function verticalMenuInit() {
    const els = document.getElementsByClassName('vertical-menu__item-name');

    const toggleClass = function (e) {
      e.preventDefault();
      const parent = this.parentElement;
      parent.classList.toggle('show-sub-item');
    };

    for (let i = 0; i < els.length; i += 1) {
      els[i].addEventListener('click', toggleClass, false);
    }
  }
  function mobileMenuInit(argument) {
    const mobileToggle = document.getElementsByClassName('top-menu__mobile-toggle');

    const toggleClass = function () {
      const horizontalMenu = this.parentElement;
      horizontalMenu.classList.toggle('show-mobile-menu');
    };

    mobileToggle[0].addEventListener('click', toggleClass, false);
  }
  verticalMenuInit();
  mobileMenuInit();
});
